package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb+srv://bfskinner:leverbox1WMy2T@cluster0.prbhq.mongodb.net/skinnerbox?authSource=admin&replicaSet=atlas-fqxzap-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass&retryWrites=true&ssl=true"))
	if err != nil {
		log.Fatal(err)
	}

	collection := client.Database("baz").Collection("qux")
	// res, err := collection.InsertOne(context.Background(), bson.M{"hello": "world"})
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println(res)

	cur, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(context.Background())
	for cur.Next(context.Background()) {
		// To decode into a struct, use cursor.Decode()
		result := struct {
			Foo string
			Bar int32
		}{}
		err := cur.Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		// do something with result...

		// To get the raw bson bytes use cursor.Current
		raw := cur.Current
		fmt.Println(raw)
		// do something with raw...
	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
}
